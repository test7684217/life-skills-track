## 1. What kinds of behaviour cause sexual harassment?

- **Verbal**: Unwanted sexual comments, jokes, propositions, or explicit remarks.
- **Non-verbal**: Displaying explicit images, obscene gestures, or suggestive messages.
- **Physical**: Unwanted touching, hugging, kissing, or physical advances.


## 2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

- Firmly tell the harasser their behavior is unwelcome.

- Talk to a trusted friend, family member, or colleague.

- Report to a supervisor, HR, school authority, or appropriate channels.

- File a complaint with regulatory bodies like the EEOC (in the U.S.).
Legal Action:

- Contact law enforcement or seek legal advice if necessary.