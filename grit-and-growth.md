## Grit and Growth Mindset


### 1. Summarize [this](https://www.youtube.com/watch?v=H14bBuluwB8) video in 1 or 2 lines.


The speaker emphasizes the importance of grit—passion and perseverance toward long-term goals—and calls for more research on developing grit in children.

### 2. Summarize [this](https://www.youtube.com/watch?v=75GFzikmRY0) video in 1 or 2 lines.

 Carol Dweck describes growth mindset as the belief that skills and intelligence can be developed through effort, learning from mistakes, embracing challenges, and being open to feedback.


### 3. What is the Internal Locus of Control? What is the key point in the [video](https://www.youtube.com/watch?v=8ZhoeSaPF-k)?

- Internal Locus of Control: believing you control your life outcomes.
- Key to staying motivated.
Feeling in control and responsible is crucial for consistent motivation.
- Developing an internal locus of control helps overcome motivation issues.

### 4. In this video, what are the key points mentioned by speaker to build growth mindset (explanation not needed).

- Believe in your ability to figure things out and get better.
- Question your assumptions and limiting beliefs.
- Develop your own life curriculum by pursuing learning in areas of passion/dreams.
- Honor the struggle when facing difficulties and challenges.

### 5. What are your ideas to take action and build Growth Mindset?

- Recognize struggle as part of the process.
- Enjoy the journey.
- Learn from failures more than from constant success.