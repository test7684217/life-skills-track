## Listening and Active Communication


### 1. What are the steps/strategies to do Active Listening? (Minimum 6 points)

1. Pay Full Attention to the speaker, avoiding outside distractions like phones or other people.
2. Demonstrate involvement, use nonverbal clues like smiling, nodding, and keeping eye contact.
3. Summarise or paraphrase the speaker's main points.
4. Let the speaker finish their point before responding with criticism or counterarguments.
5. Make sure the comments are considerate and demonstrate that one's aware of the speaker's viewpoint.
6. Thoroughly comprehend the message by asking questions if something isn't clear.

### 2. According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

According to Fisher's model, Reflective Listening involves:

1. Trying to grasp not just the words but the emotions and intentions behind them.
2. Showing that you are actively trying to understand their point of view.
3. Ask questions to clarify any ambiguous points, ensuring mutual understanding.
4. Periodically summarize the speaker's message to confirm that you’re both on the same page.
5. Acknowledge the speaker’s feelings and perspectives without necessarily agreeing, demonstrating respect for their viewpoint.

### 3. What are the obstacles in your listening process?

- Difficulty in processing and retaining complex or unfamiliar information.
- Not being engaged with the topic, leading to inattentiveness.

### 4. What can you do to improve your listening?

- Jot down key points to help retain information and show interest.
- Engage with the speaker by asking questions that clarify and expand on the topic.

### 5. When do you switch to Passive communication style in your day to day life?

- In conflict situations, to avoid confrontation or when feeling intimidated.
- Lack of energy to engage assertively, leading to passive responses.

### 6. When do you switch into Aggressive communication styles in your day to day life?

- In situations where asserting control or dominance is perceived as necessary.
- When emotions run high, and the goal shifts to winning rather than understanding.

### 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?o avoid confrontation or when feeling intimidated.

- Using sarcasm or silent treatment to express feelings of being undervalued or ignored.
- When unable to express anger directly, resorting to gossip or taunts.

### 8. How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

- Understand the type of communication style and triggers.
- Staying Calm and manage emotions to avoid aggressive responses.
- Clearly state boundaries and respect others by listening actively with empathy.
